For deploy this product you need to start local server via docker environment.

Start project with commands:
 - `docker-compose up --build`
 
After start containers. In `php` (for enter in container `docker-compose exec php bash`) container you need to run this commands:
 - `bin/console doc:data:create`
 - `bin/console doc:mig:mig`
 - `bin/console doc:fix:load`
 
Now you can go to `localhost:8001/register` and create your account.