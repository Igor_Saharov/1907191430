<?php

namespace App\Security\Voter;

use App\Entity\Area;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class AreaVoter
 *
 * @package App\Security\Voter
 */
class AreaVoter extends Voter
{
    const VIEW   = 'view';
    const EDIT   = 'edit';
    const DELETE = 'delete';

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Area) {
            return false;
        }

        return true;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        $address = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $user === $address->getUser();
            case self::EDIT:
                return $user === $address->getUser();
            case self::DELETE:
                return $user === $address->getUser();
        }

        throw new \LogicException('This Area does not have this action');
    }
}