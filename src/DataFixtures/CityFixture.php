<?php

namespace App\DataFixtures;

use App\Entity\Area;
use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class CityFixture
 *
 * @package App\DataFixtures
 */
class CityFixture extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (range(1,10) as $i) {
            $manager->persist(
                (new Area())
                    ->setName('Area: ' . $i)
            );
        }

        $manager->flush();

        $areas = $manager->getRepository(Area::class)->findAll();

        foreach ($areas as $area) {
            foreach (range(1,4) as $i) {
                $manager->persist(
                    (new City())
                        ->setName('City: ' . $i)
                        ->setArea($area)
                );
            }
        }

        $manager->flush();
    }
}
