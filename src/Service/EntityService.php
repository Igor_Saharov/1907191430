<?php

namespace App\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class EntityService
 *
 * @package App\Service
 */
class EntityService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * EntityService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param string $className
     *
     * @return ObjectRepository|null
     */
    public function getRepository(string $className): ?ObjectRepository
    {
        return $this->em
            ->getRepository($className);
    }

    /**
     * @param $object
     */
    public function fullSave($object)
    {
        $this->em->persist($object);
        $this->em->flush();
    }

    /**
     * @param $object
     */
    public function removeObject($object)
    {
        $this->em->remove($object);
        $this->em->flush();
    }
}