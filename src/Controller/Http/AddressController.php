<?php

namespace App\Controller\Http;

use App\Entity\Address;
use App\Entity\Area;
use App\Form\AddressType;
use App\Security\Voter\AddressVoter;
use App\Security\Voter\AreaVoter;
use App\Service\EntityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("addresses", name="address_")
 *
 * Class AddressController
 *
 * @package App\Controller\Http
 */
class AddressController extends AbstractController
{
    /**
     * @var EntityService
     */
    public $entityService;

    /**
     * AddressController constructor.
     *
     * @param EntityService $entityService
     */
    public function __construct(EntityService $entityService)
    {
        $this->entityService = $entityService;
    }

    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $address = (new Address())->setUser($this->getUser());
        $form    = $this
            ->createForm(AddressType::class, $address)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->denyAccessUnlessGranted(AddressVoter::EDIT, $address);

            $this->entityService->fullSave($address);

            return $this->redirectToRoute('address_index');
        }

        return $this->render('address/index.html.twig', [
            'form'      => $form->createView(),
            'addresses' => $this->entityService->getRepository(Address::class)
                                               ->findBy(['user' => $this->getUser()])
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     *
     * @param Address $address
     *
     * @return Response
     */
    public function delete(Address $address): Response
    {
        $this->denyAccessUnlessGranted(AddressVoter::DELETE, $address);

        $this->entityService->removeObject($address);

        return $this->redirectToRoute('address_index');
    }
}
