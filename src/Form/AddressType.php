<?php

namespace App\Form;

use App\Entity\Address;
use App\Entity\Area;
use App\Entity\City;
use App\Service\EntityService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddressType
 *
 * @package App\Form
 */
class AddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('area', EntityType::class, [
                'class'        => Area::class,
                'choice_label' => 'name',
                'placeholder'  => 'Select Area'
            ])
            ->add('city', EntityType::class, [
                'class'        => City::class,
                'choice_label' => 'name',
                'placeholder'  => 'Select City',
            ])
            ->add('street')
            ->add('house')
            ->add('addInfo');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Address::class
            ]);
    }
}
